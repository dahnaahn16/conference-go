import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
from django.core.mail import send_mail
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='approvedtask')
        channel.queue_declare(queue="rejectedtask")

        def presentation_approval(ch, method, properties, body):
            print("Received %r" % body)
            context = json.loads(body)
            presenter_name = context["presenter_name"]
            title = context["title"]
            presenter_email = context["presenter_email"]
            recipient_list = [presenter_email]
            send_mail(
                'Your presentation has been accepted',
                f"{presenter_name}, we are happy to tell you that your presentation {title} has been accepted",
                'admin@conference.go',
                recipient_list,
                fail_silently=False,
            )
            print("sent approval letter")

        def presentation_rejection(ch, method, properties, body):
            print("Received %r" % body)
            context = json.loads(body)
            presenter_name = context["presenter_name"]
            title = context["title"]
            presenter_email = context["presenter_email"]
            recipient_list = [presenter_email]
            send_mail(
                'Your presentation has not been approved',
                f"{presenter_name}, we are sad to tell you that your presentation {title} has not been accepted",
                'admin@conference.go',
                recipient_list,
                fail_silently=False,
                )
            print("sent rejection letter")

        channel.basic_consume(
            queue='approvedtask',
            on_message_callback=presentation_approval,
            auto_ack=True,
        )

        channel.basic_consume(
            queue='rejectedtask',
            on_message_callback=presentation_rejection,
            auto_ack=True,
        )
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
